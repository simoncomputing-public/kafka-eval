package com.example.kafka.recv;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import com.example.kafka.recv.Receiver;

@Configuration
@EnableKafka
public class KafkaReceiverConfig {

  @Value("${kafka.bootstrap-servers}")
  private String bootstrapServers;
  
  @Value( "${ssl.enabled}")
  private boolean sslEnabled;  

  @Bean
  public Map<String, Object> consumerConfigs() {
    Map<String, Object> props = new HashMap<>();
    // list of host:port pairs used for establishing the initial connections to the Kakfa cluster
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

    // allows a pool of processes to divide the work of consuming and processing records
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer-group-1");

    if ( sslEnabled ) {
        props.put( "security.protocol", "SSL" );
        props.put("ssl.keystore.location", "./client.keystore.jks" );
        props.put("ssl.truststore.location", "./server.truststore.jks" );
        props.put("ssl.keystore.password" , "tester" );
        props.put("ssl.truststore.password" , "tester" );

        props.put( "ssl.key.password", "tester" );
        props.put( "ssl.truststore.type", "JKS" );
        props.put( "ssl.keystore.type", "JKS" );
    }

    return props;
  }

  @Bean
  public ConsumerFactory<String, String> consumerFactory() {
    return new DefaultKafkaConsumerFactory<>(consumerConfigs());
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    factory.setConcurrency(1);
	factory.getContainerProperties().setPollTimeout(3000);  // 1414
	factory.getContainerProperties().setIdleEventInterval(5000L);
    
    return factory;
  }

  @Bean
  public Receiver receiver() {
    return new Receiver();
  }
}
