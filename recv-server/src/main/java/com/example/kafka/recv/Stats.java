package com.example.kafka.recv;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Keeps track of statistics and logs them as appropriate. 
 */
public class Stats {
    final static Logger stats = LoggerFactory.getLogger( "stats" );
    final static Logger trace = LoggerFactory.getLogger( "trace" );
    
    // accumulated totals per second
    private int msgCount;
    private int msgSize;

    // Timestamp by second
    private String time;
    
    private SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    
    public void start() {
    		time = sdf.format( Calendar.getInstance().getTime() );
    		stats.debug( "Time, Count, Cumulative Size, Avg Size" );
    		trace.trace( "Time, ID, Hash, Size" );
    }
    
    // Log all trace & stats information
    public synchronized void log( String id, String hash, int size ) {
        String timeStamp = sdf.format( Calendar.getInstance().getTime() );      

        trace.trace( timeStamp + ", " + id + ", " + hash + ", " + size );
        if ( !timeStamp.equals( time ) ) 
        		flush( timeStamp );
        
        msgCount++;
        msgSize += size;
        
    }
    
    public synchronized void flush() {
        String timeStamp = sdf.format( Calendar.getInstance().getTime() );
        flush( timeStamp );
    }
    
    private void flush( String timeStamp ) {
    		if ( msgCount < 1 ) return;
    		
        stats.debug( time + ", " + ( msgCount ) + ", " + msgSize + ", " + ( msgCount == 0 ? 0 : msgSize/msgCount ) );
        
        time = timeStamp;
        msgCount = 0;
        msgSize  = 0;
    	
    }
    
    public void debug( String value ) {
        stats.debug( value );
    }
    
}

