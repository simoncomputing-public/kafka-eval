package com.example.kafka.recv;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.event.ListenerContainerIdleEvent;

public class Receiver {

  private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);
  private Stats stats = new Stats();
 
  public Receiver() {
	  stats.start();
  }
  
  @KafkaListener(topics = "TestTopic", group="consumer-group-1" )
  public void receive(String json) {
      
      try {
          JSONObject obj = new JSONObject( json );
          String id = obj.getString( "messageId" );
          stats.log( id, Long.toString( 0xffffffffL & json.hashCode(), 16), json.length() );
      } catch ( JSONException e ) {
        LOGGER.error( e.getMessage(), e );
    }

  }
  
  @EventListener()
  public void eventHandler(ListenerContainerIdleEvent event) {
	  stats.flush();
  }
}
