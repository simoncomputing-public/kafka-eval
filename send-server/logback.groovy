// -DLogback.configurationFile=/path/to/logback.groovy

def LOG_PATH = "/var/log/kafka-send"
def ARCHIVE_PATH = "/var/log/archive"

context.name="Test"
addInfo("Context name has been set to ${context.name}")
statusListener( OnConsoleStatusListener )

scan( "30 seconds" )

appender( "console-appender", ConsoleAppender ) {
    encoder( PatternLayoutEncoder ) {
        pattern = "%level %logger = %msg%n"
    }
}

appender( "message-appender", RollingFileAppender ) {
    file = "${LOG_PATH}/kafka-message.log"
    append = true
    rollingPolicy( TimeBasedRollingPolicy ) {
        fileNamePattern = "${ARCHIVE_PATH}/kafka-message.%d.log"
        maxHistory = 30
        totalSizeCap = "1GB"
    }

    encoder( PatternLayoutEncoder ) {
        pattern = "%d %level %logger %msg%n"
    }
}


appender( "root-appender", RollingFileAppender ) {
    file = "${LOG_PATH}/kafka-send-root.log"
    append = true
    rollingPolicy( TimeBasedRollingPolicy ) {
        fileNamePattern = "${ARCHIVE_PATH}/kafka-send-root.%d.log"
        maxHistory = 30
        totalSizeCap = "1GB"
    }

    encoder( PatternLayoutEncoder ) {
        pattern = "%level %logger = %msg%n"
    }
}

appender( "stats-appender", RollingFileAppender ) {
    file = "${LOG_PATH}/kafka-send-stats.csv"
    append = true

    rollingPolicy( TimeBasedRollingPolicy ) {
        fileNamePattern = "${ARCHIVE_PATH}/kafka-send-stats.%d.csv"
        maxHistory = 30
        totalSizeCap = "1GB"
    }

    encoder( PatternLayoutEncoder ) {
        pattern = "%msg%n"
    }
}

appender( "trace-appender", RollingFileAppender ) {
    file = "${LOG_PATH}/kafka-send-trace.csv"
    append = true

    rollingPolicy( TimeBasedRollingPolicy ) {
        fileNamePattern = "${ARCHIVE_PATH}/kafka-send-trace.%d.csv"
        maxHistory = 30
        totalSizeCap = "1GB"
    }

    encoder( PatternLayoutEncoder ) {
        pattern = "%msg%n"
    }
}

root( info, ["root-appender"] )
logger( "stats", DEBUG, ["stats-appender"], false )
logger( "message", DEBUG, ["message-appender"], false )
