package com.example.kafka.send;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

public class Sender {

  private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

  @Autowired
  private Stats stats;

  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  public void send(int id, String topic, String message) {
    // the KafkaTemplate provides asynchronous send methods returning a Future
    ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, message);

    // register a callback with the listener to receive the result of the send asynchronously
    future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

      @Override
      public void onSuccess(SendResult<String, String> result) {
        LOGGER.trace("sent message='{}' with offset={}", message, result.getRecordMetadata().offset());
    
      }

      @Override
      public void onFailure(Throwable ex) {
        LOGGER.error("unable to send message='{}'", message, ex);
        stats.trace( "" + id + ", 0x" + Long.toString( 0xffffffffL & message.hashCode(), 16) + ", " + message.length() + ", Send Failed" );
        
        // Pause to slow down retries...
        try { Thread.sleep( 5000 ); } catch (InterruptedException e) {}

      }
    });

    
    // or, to block the sending thread to await the result, invoke the future's get() method
  }
}
