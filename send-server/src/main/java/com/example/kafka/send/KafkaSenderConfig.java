package com.example.kafka.send;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.example.kafka.send.Sender;

@Configuration
public class KafkaSenderConfig {

  @Value("${kafka.bootstrap-servers}")
  private String bootstrapServers;
  
  @Value( "${ssl.enabled}")
  private boolean sslEnabled;

  @Bean
  public Map<String, Object> producerConfigs() {
    Map<String, Object> props = new HashMap<>();

    // list of host:port pairs used for establishing the initial connections to the Kakfa cluster
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

    if ( sslEnabled ) {
        props.put( "security.protocol", "SSL" );
        props.put("ssl.keystore.location", "./client.keystore.jks" );
        props.put("ssl.truststore.location", "./server.truststore.jks" );
        props.put("ssl.keystore.password" , "tester" );
        props.put("ssl.truststore.password" , "tester" );

        props.put( "ssl.key.password", "tester" );
        props.put( "ssl.truststore.type", "JKS" );
        props.put( "ssl.keystore.type", "JKS" );
    }
    return props;
  }

  @Bean
  public ProducerFactory<String, String> producerFactory() {
    return new DefaultKafkaProducerFactory<>(producerConfigs());
  }

  @Bean
  public KafkaTemplate<String, String> kafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }

  @Bean
  public Sender sender() {
    return new Sender();
  }
  
}
