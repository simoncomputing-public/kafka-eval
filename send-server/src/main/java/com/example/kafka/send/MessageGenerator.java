package com.example.kafka.send;

import java.io.ByteArrayOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.scheduling.annotation.Async;

import org.springframework.stereotype.Service;

import com.example.kafka.send.Sender;
import com.github.vincentrussell.json.datagenerator.JsonDataGeneratorException;
import com.github.vincentrussell.json.datagenerator.impl.JsonDataGeneratorImpl;


/**
 * A generator utilizing a JSON generator from 
 * https://github.com/vincentrussell/json-data-generator
 *
 */
@Service
public class MessageGenerator {
    final static Logger logger = LoggerFactory.getLogger( "message" );
    
    private String topic = "TestTopic";
    
    @Autowired 
    private Stats stats;

    @Autowired
    private Sender sender;

    private static int count;
    private boolean stop;

    private int burstInterval = 0;
    private int msgsPerBurst  = 1000;
    private int multiple      = 1;
    private int pauseInterval = 5000;
    
    JsonDataGeneratorImpl parser = new JsonDataGeneratorImpl();
    ByteArrayOutputStream os = new ByteArrayOutputStream( 10000 );
    String pattern = "{ " 
            + "\"messageId\": \"%d\", \n" 
            + "\"content\": [ '{{repeat(%d)}}',"
            + "{\"email\": \"{{email()}}\", \n" 
            + "\"ssn\": \"{{ssn()}}\", \n"
            + "\"gender\": \"{{gender()}}\", \n" 
            + "\"first\": \"{{firstName()}}\", \n" 
            + "\"last\": \"{{lastName()}}\", \n"
            + "\"address\": [ '{{repeat(3)}}', "
            + "  {" 
                    + "\"city\": \"{{city()}}\", " 
                    + "\"state\": \"{{state()}}\", "
                    + "\"zip\": \"{{integer(10000, 99999)}}\", "
                    + "\"country\": \"{{country()}}\" " 
               + "}" 
            + "], \n" 
            + "\"ipAddr\": \"{{ipv4()}}\", \n"
            + "\"timestamp\": \"{{timestamp()}}\", \n" 
            + "\"usCitizen\": \"{{bool()}}\",\n" 
            + "\"uuid\": \"{{uuid()}}\",\n"
            + "\"comments\": \"{{lorem(70,\"words\")}}\"\n" + "}]"
            + "}";

    // @Autowired
    // private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    @Async
    public void run() {
        stop = false;
        stats.start();
        
        while ( !stop ) {
            try {

                for ( int i=0; i<msgsPerBurst; i++ ) {
                    
                    // Generate JSON Data
                    os.reset();
                    String value = String.format( pattern, count, multiple );
                    parser.generateTestDataJson( value, os );
                    String json = os.toString();
                    
                    // Send it
                    sender.send( count, topic, json );
                    
                    // Record each send
                    stats.log( "" + count, "0x" + Long.toString( 0xffffffffL & json.hashCode(), 16), os.size() );
                    count++;
                }
                Thread.sleep( burstInterval );

            } catch ( InterruptedException e ) {
            		pause();
            		logger.error( e.getMessage(), e );
            } catch ( JsonDataGeneratorException e ) {
            		stop = true;
	            	logger.error( e.getMessage(), e );
            } catch( Throwable e ) {
            		stop = true;
	            	logger.error( e.getMessage(), e );
            }

        }
        stats.stop();
        logger.debug( "Generator stopped." );
    }

    public void pause() {
    		try {
				Thread.sleep( pauseInterval );
			} catch (InterruptedException e) {
				// do nothing
			}
    		
    }
    
    public void stop() {
        stop = true;
    }

    public int getBurstInterval() {
        return burstInterval;
    }

    public void setBurstInterval( int burstInterval ) {
        stats.debug( "burstInterval=" + burstInterval );
        this.burstInterval = burstInterval;
    }

    public int getMsgsPerBurst() {
        return msgsPerBurst;
    }

    public void setMsgsPerBurst( int msgsPerBurst ) {
        stats.debug(  "msgsPerBurst=" + msgsPerBurst );
        this.msgsPerBurst = msgsPerBurst;
    }

    public int getMultiple() {
        return multiple;
    }

    public void setMultiple( int multiple ) {
        this.multiple = multiple;
    }

	public int getPauseInterval() {
		return pauseInterval;
	}

	public void setPauseInterval(int pauseInterval) {
		this.pauseInterval = pauseInterval;
	}


}



