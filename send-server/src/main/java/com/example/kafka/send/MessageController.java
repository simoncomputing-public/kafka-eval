package com.example.kafka.send;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class MessageController {
    
    @Autowired
    MessageGenerator generator;
    
    @RequestMapping( value="/setup", method=RequestMethod.POST )
    public String setup( @RequestParam( "burstInterval" ) int burstInterval, 
                         @RequestParam( "msgsPerBurst" ) int msgsPerBurst,
                         @RequestParam( "multiple" ) int multiple ) {
        
        generator.setBurstInterval(  burstInterval );
        generator.setMsgsPerBurst(  msgsPerBurst );
        generator.setMultiple( multiple );

        return null;
    }
    
    @RequestMapping( value="/start", method=RequestMethod.GET )
    public String startGenerator() { 
        generator.run();
        return "Generator started.";
    }
    
    @RequestMapping( value="/stop", method=RequestMethod.GET )
    public String stopGenerator() {
        generator.stop();
        return "Generator stop requested.";
    }
}
