package com.example.kafka.send;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class SendApplication {
    final static Logger logger = LoggerFactory.getLogger( "message" );
    
    public static void main( String[] args ) {
        logger.info( "Application started at: " + System.getProperty("user.dir") );
        SpringApplication.run( SendApplication.class, args );
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // Swagger and Spring Fox Configuration
    // add this helper method
    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("KafkaSender API")
            .description("Kafka REST API")
            .license("MIT license")
            .licenseUrl("https://opensource.org/licenses/MIT")
            .termsOfServiceUrl("")
            .version("1.0.0")

            .build();
        }


    // Add this bean
    @Bean
    public Docket api() {
        return new Docket( DocumentationType.SWAGGER_2 ).select()
                .apis( RequestHandlerSelectors.basePackage( "com.example.kafka" ) )
                .build()
                .apiInfo( apiInfo() );
    }


}
