burstInterval=5000
msgPerBurst=3
multiple=8
replication-factor=3
num.partitions=2
min.in-sync-replica=2

senders: 2
receivers: 2