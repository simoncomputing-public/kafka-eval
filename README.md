# Kafka Evaluation

## Overview

This evaluation specifically tests Kafka's Publish & Subscribe capability.  This is where a messages producer **publishes** messaeges 
to a topic and interested applications **subscribe** to specific topics.   One or more can subscribe to a topics and listen to the 
stream of messages that are being published to it.  

This capability is very similar to message queues.   However Kafka operates like a queue and messages are intended to be retrieved 
First in, First out (FIFO).   It is very simple in operation and does not have features like searching for a specific message on the queue.   
However, this should not be viewed as a handicap.  This allows the tool to operate very fast and it just requires designing logic so that 
it does not depend on search and retrieving messages out of sequence - these features have serious performance penalties and should not be 
used in high volume environments anyways. 

A notable difference with Kafka from message queues is that messages are not disposed as they are being read.  Instead a pointer is simply 
moved to the next message.  This makes it possible to replay messages by moving the pointer back.   

## Recommended Usage

Kafka is an **asynchronous** messaging tool.   A message producer does not wait for a response after it publishes a message.   It is highly suited 
to situations where a high volume of messages need to be transferred to be processed later.   Often developers call this throwing messages "over
the fence."   Capturing audit log information from multiple applications and transferring large amounts of data for processing are ideal scenarios.


While applications have been written to use **asynchronous** protocols to performa transactional actions, this is strongly discouraged because **async** 
protocols cannot guarantee a confirmed pass/failed condition.   It is possible to assume an error because the calling application timed out waiting for a
response and the response simply arrived late. 

If the application needs **synchronous** behavior, such as getting a success indicator when submitting forms, you should be using a **synchronous** protocol 
such as SOAP or REST Web Services.   These protocols block the caller from continuing until the message is processed and a success/fail indicator is 
passed back to the caller.   

## Purpose of Tests

The purpose of these projects is to evaluate Kafka as a tool to aggregate logging information coming from multiple servers
to a Kafka cluster.  A separate set of consumer servers will pull messages of for storage.   

We are setting out to answer the following questions: 

1. How much throughput a cluster of servers handle?
   - How much does each additional server add to capacity?
   - What is the impact to performance when we add partitions?
   - What is the impact of performance when we add more client servers that send messages?
   - What is the impact of performance when we add consumers that read messages? 

2. What happens during failure? 
   - How does the cluster react when a node is removed?
   - How does the cluster react when a node is added? 
   - How does the cluster react when space runs out? 
   - What are the warning indications for failure? 
   - Can messages that are not read be aged off?   What can we do to protect against this?

## Testing Setup 

All servers will operate within the AWS environment.   The test will be composed of the following elements: 

1. A Kafka cluster with one or more nodes.
1. A client application that generates random JSON based messages.   There will be one or more nodes running
   the client application.  
   - The client application will identify each message with a message ID, size of message
   and hash of message in a log so we can track how long it is taking to write data.   We'll also be able to verify 
   that the messages were recieved using these logs.  
   - The clent application will allow the rate of messages delivered to be adjusted via user interface.
1. A consumer application that pulls off the messages from Kafka.   The messages will not be stored but the message ID,
   time of reciept, size of and hash will be logged.

Logging will be formatted as CSV files for easy analysis in Excel. 

### Performance Testing

All tests were run on Amazon Cloud using an Amazon Linux AMI with 8GB SSD using the t2.medium type server.   A Kafka cluster of 3 nodes and a separate ZooKeeper node was used in each test run.

### Run A: Single Receiver and Sender Test

- Sender Cluster: 1
- Receiver Cluster: 1
- Avg Msg Size: 8,363
- Partitions: 1
- Replication Factor 1

#### Run A: Sender Stats
![run1-sender](./resources/run1/run1-sender.png)

#### Run A: Receiver Status
![run1-receiver](./resources/run1/run1-receiver.png)

### Run B: Multiple Node Sender/Receiver, Minimum Insync Replicas: 1

- Sender Cluster: 2
- Receiver Cluster: 2
- Avg Msg Size: 8,363
- Partitions: 2
- Replication Factor: 3
- min.insync-replica: 1

This tested with multiple node generating data and consuming data.  In addition, 2 partitions were created on Kafka to facilitate multi-tasking.  Only one replica needed to be successfully written for the write to be considered successful.

#### Run B: Sender1 Stats

![run4-sender1](./resources/run4/run4-sender1.png)

#### Run B: Sender2 Stats

![run4-sender2](./resources/run4/run4-sender2.png)

### Run B: Receiver1 Stats
![run4-receiver1](./resources/run4/run4-receiver1.png)

### Run B: Receiver2 Stats
![run4-receiver2](./resources/run4/run4-receiver2.png)


## Run C: Multilpe Node Sender/Receiver, Minimum Insync Replicas: 2

- Sender Cluster: 2
- Receiver Cluster: 2
- Avg Msg Size: 8,363
- Partitions: 2
- Replication Factor: 3
- min.insync-replica: 2

This tests withe same test as Run B but requires two replicas to be updated before a write is considered successful.

#### Run C: Sender1 Stats

![run3-sender1](./resources/run3/run3-sender1.png)

#### Run C: Sender2 Stats

![run3-sender2](./resources/run3/run3-sender2.png)

### Run C: Receiver1 Stats
![run3-receiver1](./resources/run3/run3-receiver1.png)

### Run C: Receiver2 Stats
![run3-receiver2](./resources/run3/run3-receiver2.png)


## Run D: 108K Message Size

- Sender Cluster: 2
- Receiver Cluster: 2
- Avg Msg Size: 8,363
- Partitions: 2
- Replication Factor: 3
- min.insync-replica: 2

Same test was run as Run C but with 100K messages.

#### Run D: Sender1 Stats

![run6-sender1](./resources/run6/run6-sender1.png)

#### Run D: Sender2 Stats

![run6-sender2](./resources/run6/run6-sender2.png)

### Run D: Receiver1 Stats
![run6-receiver1](./resources/run6/run6-receiver1.png)

### Run D: Receiver2 Stats
![run6-receiver2](./resources/run6/run6-receiver2.png)


### Reliability

We setup a cluster with 3 nodes and replicated data across those nodes.   It continued to operate without missing a beat when we took down one and then two nodes.  As long as there was a single node running, everything continued to operate.   Restoring functionality was as easy as turning the nodes back on or standing up 
new nodes with the same server addresses.

The one thing that still needs to be tested is recovering from running out of space.  

### Maintainability

Ongoing maintenance will require monitoring the nodes for warning signs such as rapidly growing disk usage, data aging off before being processed and CPU usage.   
We'll also want to make sure that the message consumers are not being outpaced by message producers.

### Ease of Use

This is a moderately complex tool because of the manual configuration steps, but it is manageable.

The tutorials are very helpful and get you up and running quickly.  But you do need to be proficient wth Unix and very thoughtful
about what you are doing.    There is a lot of manual updating of configuration files and you can mess it up very easily by 
mistyping something.   You also need to understand certificate management with the **keytool** command that comes with Java.   If you can 
understand what the intent of the commands in Appendix A, then you will be fine. 

Standard Operating Procedures or automated scripts specific to your team or organization will go far to making setup and maintenance 
of Kafka a repeatable and reliable process. 


## Lessons Learned

  1. It's best to define the topics rather than have them automatically created when you first reference them.  It doesn't seem to work well unless the receiver starts first.   
 
 ## To Do

 1. Evaluate specific volume scenarios to make sure that we have sized the 
 servers properly based on the volume and the time-to-live for messages. 

 1. Figure out how to run with a cluster of zookeepers.

 1. Figure out how to recover from running out of disk space.


## Appendix A - Key Creation Commands

```shell

# Create CA Cert keys
keytool -keystore kafka1.keystore.jks -alias kafka1 -validity 365 -genkey

# Export the certificate file you just created to cert-file
keytool -keystore kafka1.keystore.jks -alias kafka1 -certreq -file cert-file

# Sign the certificate file with the CA key, resulting in cert-signed
openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file -out cert-signed -days 365 -CAcreateserial -passin pass:tester

# Import both the CA certificate and the signed file back into the keystore
keytool -keystore kafka1.keystore.jks -alias CARoot -import -file ca-cert
keytool -keystore kafka1.keystore.jks -alias kafka1 -import -file cert-signed
```