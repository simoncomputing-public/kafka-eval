# Setup of Zoo and Kafka Servers

This is a quick test environment setup.  These instructions need more development for permanent setups.

## Initial Installation

1. Create an EC2 medium instance with default values
1. All servers must support SSH.
1. The security group for Zookeeper needs to open port 2181 to anything in the same VPC.  See the default **Load Balancer** security group to get the **source IP** to use.
1. The security group for Kafka needs to open port 19092 to all servers from your IP and also from the same VPC.  Note that this port is changed from the default of 9092 to support SSL.
1. Change the owner of multiple directories to **ec2-user**:

```shell
# Download Zookeeper/Kafka installation
cd /opt
sudo wget http://apache.claz.org/kafka/0.10.2.0/kafka_2.11-0.10.2.0.tgz

# Extract it
sudo tar xzf kafka_2.11-0.10.2.0.tgz 

# Simplify working on the server by changing ownership of the following directories:
sudo chown -R ec2-user kafka_2.11-0.10.2.0/
sudo chown ec2-user /etc/hosts
sudo chown ec2-user /tmp
```

## Copy Certs and Configs

This directory in git contains additional files for the Kafka installations that go in the **/certs** and **/config** directories:

Go to this git repository's config/kafka directory and run the following commands to copy the contents to your server:

```shell
scp -rp certs/ ec2-user@kafka1:certs/ 
scp -rp config/ ec2-user@kafka1:config/ 
```

1. Copy **/certs** into the Kafka installation directory.
1. Copy **/config** into the Kafka **config** directory.   
1. In the Kafka config directory, rename the original server.properties file and replace it with the server config of one of the instances.  For example:

```shell
mv server.properties server.properties.orig
mv server.1.properties server.properties
```

## Update /etc/hosts

Update entries in /etc/hosts to have the following servers:
 - zookeeper1
 - zookeeper2
 - kafka1
 - kafka2
 - kafka3
 
 ## Starting the servers

 This directory contains the startup script for zookeeper and kafka.   You should only install **zoo** on the Zookeeper instances and **kafka** on the Kafka instances.  This will prevent accidentally running the wrong application on each instance.

 ```shell

 # To start Zookeeper
 cd ~
 ./zoo

 # To start Kafka
 cd ~
 ./kafka
 ``` 

## Stopping the server

You can stop the server with Control-C.   If this does not work, run ```ps -aux``` to find the process ID of the server and then run ```kill -9 <process ID>```.

 ## Resetting the servers

 Whenever you want to reset the servers, shutdown the server and delete everything under the **/tmp** directory:

 ```shell
 sudo rm -rf /tmp/*
 ```

 

